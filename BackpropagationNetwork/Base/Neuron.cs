﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpropagationNetwork.Base
{

    public class Neuron
    {
        public double Input { get; set; }
        public double Output { get => Activation(Input); }
        public double Delta { get; set; }
        public Func<double, double> Activation { get; set; }
        public List<Weight> InputWeight { get; set; }
        public List<Weight> OutputWeight { get; set; }
        public Weight Bias { get; set; }
        public Neuron()
        {
            InputWeight = new List<Weight>();
            OutputWeight = new List<Weight>();
        }

    }
}
