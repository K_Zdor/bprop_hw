﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpropagationNetwork.Base
{
    public class CalculatingError
    {
        public double MSE(double[] real, double[] expected)
        {
            var sum = 0.0;
            var setsCount = real.Count();
            for (int i = 0; i < setsCount; i++)
            {
                sum += Math.Pow(real[i] - expected[i], 2);
            }
            return sum / setsCount;
        }

        public double RootMSE(double[] real, double[] expected)
        {
            return Math.Sqrt(MSE(real, expected));
        }

        public double Arctan(double[] real, double[] expected)
        {

            var sum = 0.0;
            var setsCount = real.Count();
            for (int i = 0; i < setsCount; i++)
            {
                sum += Math.Pow(Math.Atan(real[i] - expected[i]), 2);
            }
            return sum / setsCount;
        }
    }
}
