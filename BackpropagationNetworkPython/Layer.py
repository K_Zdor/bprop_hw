from Neuron import Neuron
from Weight import Weight
from ActivationFunctions import ActivationFunctions
import random


class Layer:
    def __init__(self, neurons_count, bias, activation):
        self.neurons = []
        self.act_func = ActivationFunctions()
        if activation is None:
            activation = self.act_func.sigmoid
        self.activation = activation
        for i in range(neurons_count):
            self.neurons.append(Neuron(activation))
        if bias == True:
            self.bias = Neuron(self.act_func.line)
            self.bias.input = 1
        else:
            self.bias = None

    def connect_to_next_layer(self, layer):
        rnd = random.Random(10)
        for end_neuron in layer.neurons:
            for start_neuron in self.neurons:
                weight = Weight(start_neuron, end_neuron)
                weight.value = rnd.random()
                end_neuron.input_weight.append(weight)
                start_neuron.output_weight.append(weight)
            if self.bias != None:
                weight_bias = Weight(self.bias, end_neuron)
                weight_bias.value = rnd.random()
                end_neuron.input_weight.append(weight_bias)
                self.bias.output_weight.append(weight_bias)



if __name__=='__main__':
    first_layer = Layer(3, True, lambda x:x)
    second_layer = Layer(5, True, lambda x:x)
    first_layer.connect_to_next_layer(second_layer)
    print('first part')
    for neuron in first_layer.neurons:
        print('neuron_inp')
        for weight in neuron.output_weight:
            print('weight val = ', weight.value,'neuron_out', weight.end != None)
    print()
    print('second part')
    for neuron in second_layer.neurons:
        print('neuron_out')
        for weight in neuron.input_weight:
            print('weight val = ', weight.value,'neuron_in', weight.start != None)
            