from Neuron import Neuron
from Weight import Weight
from ActivationFunctions import ActivationFunctions
from CalculatingError import CalculatingError
from Layer import Layer

class BackpropagationNetwork:
    def __init__(self):
        self.calc_error = CalculatingError()
        self.layers = []
        self.learning_rate = 0.01
        self.momentum = 0.3

    def input_layer(self):
        return self.layers[0]
    def output_layer(self):
        return self.layers[-1]
    def hidden_layers(self):
        return self.layers[1:-1]
    def layers_count(self):
        return len(self.layers)
    def connect_layers(self):
        for i in range(self.layers_count() - 1):
            self.layers[i].connect_to_next_layer(self.layers[i + 1])
    def calculate_neuron(self, neuron):
        pass

    def calculate_output(self, inputs=None):
        if inputs != None:
            for i in range(len(self.input_layer().neurons)):
                self.input_layer().neurons[i].input = inputs[i]
        for i in range(1,len(self.layers)):
            for neuron in self.layers[i].neurons:
                self.calculate_neuron(neuron)

    def calculate_error(self, expected):
        real = [ output.output() for output in self.output_layer().neurons]
        error = self.calc_error.rootMSE(real, expected)
        return error

    def train(self, inputs, expected, epochs, writer):
        for epoch in range(epochs):
            epoch_error = []
            sets = len(inputs)
            for set in range(sets):
                input = [float(x) for x in inputs[set].split(' ')]
                expected_result = [float(x) for x in expected[set].split(' ')]
                for i in range(len(self.input_layer().neurons)):
                    self.input_layer().neurons[i].input = input[i]
                self.calculate_output()
                result = [x.output() for x in self.output_layer().neurons]
                error = self.calc_error.rootMSE(result, expected_result)
                epoch_error.append(error)
                self.weights_calibration(expected_result)
            mid_error = sum(epoch_error) / len(epoch_error)
            if (epoch + 1) % 5 == 0:
                if writer != None:
                    writer("Epoch",epoch + 1," ; error = ",mid_error,"%")
            if mid_error < 0.05:
                writer("Epoch",epoch + 1," ; error = ",mid_error,"%")
                
    def weights_calibration(self, expected):
        for i in range(len(self.output_layer().neurons)):
            self.calculate_delta_output_neuron_sigmoid(self.output_layer().neurons[i], expected[i])
        for neuron in self.output_layer().neurons:
            for weight in neuron.input_weight:
                weight.calc_delta_weight(self.learning_rate, self.momentum)
                weight.update_weight()

        self.hidden_layers().reverse()
        reverse_hidden_layers = self.hidden_layers()
        for layer in reverse_hidden_layers:
            for i in range(len(layer.neurons)):
                self.calculate_delta_hidden_neuron_sigmoid(layer.neurons[i])
            for neuron in layer.neurons:
                for weight in neuron.input_weight:
                    weight.calc_delta_weight(self.learning_rate, self.momentum)
                    weight.update_weight()
        self.hidden_layers().reverse()

    def calculate_delta_output_neuron_sigmoid(self, neuron, expected):
        neuron.delta = (expected - neuron.output())* self.derivative_sigmoid(neuron)

    def calculate_delta_hidden_neuron_sigmoid(self, neuron):
        pass

    def derivative_sigmoid(self, neuron):
        return (1 - neuron.output())*neuron.output()


if __name__=='__main__':
    bprop = BackpropagationNetwork()
    activ_func = ActivationFunctions()
    input_layer = Layer(3, True, activ_func.line)
    hidden_layer1 = Layer(12, True, activ_func.sigmoid)
    hidden_layer2 = Layer(12, True, activ_func.sigmoid)
    output_layer = Layer(4, False, activ_func.sigmoid)
    bprop.layers.append(input_layer)
    bprop.layers.append(hidden_layer1)
    bprop.layers.append(hidden_layer2)
    bprop.layers.append(output_layer)
    bprop.connect_layers()
    for layer in bprop.layers:
        print('neuron count = ', len(layer.neurons), 'weight count = ', sum([len(x.output_weight) for x in layer.neurons]))
    print('input layer neurons count = ', len(bprop.input_layer().neurons))
    print('hidden layers count = ', len(bprop.hidden_layers()))
    print('output layer neurons count = ', len(bprop.output_layer().neurons))
    inputs = [3,4,2]
    bprop.calculate_output(inputs)
    print('input = ', [x.output() for x in bprop.input_layer().neurons])
    print('output = ', [x.output() for x in bprop.output_layer().neurons])
    bprop.calculate_error([1,0,0,0])
    inputs = ['1 2 3','1 2 4','1 3 4','2 3 4','1 2 3']
    expected = ['0 0 0 1','0 1 0 0','1 0 0 0','0 0 1 0','0 0 0 1']
    bprop.train(inputs, expected, 5, print)






