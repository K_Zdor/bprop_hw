import math

class CalculatingError:
    def mse(self, real, expected):
        sum = 0
        setsCount = len(real)
        for i in range(setsCount):
            sum += math.pow(real[i]-expected[i],2)
        return sum / setsCount

    def rootMSE(self, real, expected):
        return math.sqrt(self.mse(real,expected))

    def arctan(self, real, expected):
        sum = 0
        setsCount = len(real)
        for i in range(setsCount):
            sum += math.pow(math.atan(real[i]-expected[i]), 2)
        return sum/setsCount

if __name__=='__main__':
    calc_err = CalculatingError()
    a = [1,2,3]
    b = [1,3,3]
    print('mse ', a, b, ' = ', calc_err.mse(a,b))
    print('root mse ', a, b, ' = ', calc_err.rootMSE(a,b))
    print('arctan ', a, b, ' = ', calc_err.arctan(a,b))