﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BackpropagationNetwork.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpropagationNetwork.Base.Tests
{
    [TestClass()]
    public class WeightTests
    {
        [TestMethod()]
        public void GRADTest()
        {
            var input = 2;
            var delta = 3;
            var neuronStart = new Neuron() { Input = input, Activation = x => x, Delta = 99999 };
            var neuronEnd = new Neuron() { Delta = delta, Activation = x => x, Input = 99999 };
            var weight = new Weight() { Start = neuronStart, End = neuronEnd, Value = 99999 };
            var real = weight.GRAD();
            var expected = input * delta;

            Assert.IsTrue(real == expected);
        }

        [TestMethod()]
        public void CalcDeltaWeightTest()
        {
            var learningRate = 0.1;
            var momentum = 0.1;
            var input = 2;
            var delta = 3;
            var neuronStart = new Neuron() { Input = input, Activation = x => x, Delta = 99999 };
            var neuronEnd = new Neuron() { Delta = delta, Activation = x => x, Input = 99999 };
            var weight = new Weight() { Start = neuronStart, End = neuronEnd, Value = 99999 };
            

            var expected = learningRate * weight.GRAD();
            weight.CalcDeltaWeight(learningRate, momentum);
            var real = weight.DeltaWeight;

            Assert.IsTrue(real==expected);
        }
    }
}