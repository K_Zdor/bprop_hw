﻿using BackpropagationNetwork.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackpropagationNetworkTests.Base
{
    [TestClass()]
    public class NeuronTests
    {
        [TestMethod()]
        public void ActivateNeuronTest()
        {
            var activationFunctions = new ActivationFunctions();
            var neuron = new Neuron() { Input = 100, Activation = activationFunctions.Sigmoid };
            var real = neuron.Output;
            var expected = activationFunctions.Sigmoid(neuron.Input);
            Assert.IsTrue(real == expected);
        }
    }
}
